#include <iostream>
#include "ABR.hpp"
#include <cstdlib>

int main()
{
    ABR<int> tree;
    for (int i = 0; i < 10; i++){
        tree.insert(rand() % 100); 
    }   
    ABR<int>::Iterator it=tree.begin();
    ABR<int>::Iterator ite=tree.end();
    for(;it!=ite;++it)
        std::cout << *it << std::endl;


    return 0;
}