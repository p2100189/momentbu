#include <iostream>
#include "ABR.hpp"
#include <cstdlib>

int main()
{
    ABR<int> tree;
    for (int i = 0; i < 10; i++)
    {
        tree.insert(rand() % 100);
    }
    std::cout << tree << std::endl;
    while (!tree.find(rand() % 100))
        ;

    Noeud<int> n1;
    std::cout << n1.getInfo() << std::endl;

    ABR<int>::iterator it = tree.begin();
    ABR<int>::iterator ite = tree.end();
    for (; it != ite; ++it)
        std::cout << *it << std::endl;

    return 0;
}