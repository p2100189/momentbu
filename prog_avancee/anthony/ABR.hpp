#ifndef ABR_H
#define ABR_H

#include <iostream>
#include <stack>

template <typename T>
class Noeud
{
private:
    T info;
    Noeud<T> *fg;
    Noeud<T> *fd;

public:
    Noeud() : info(T()), fg(nullptr), fd(nullptr) {}

    Noeud(const T & newInfo) : info(newInfo), fg(nullptr), fd(nullptr) {}

    Noeud(const Noeud<T> &n) : info(n.info)
    {
        if (n.fg == nullptr)
        {
            this->fg = nullptr;
        }
        else
        {
            this->fg = new Noeud<T>(*n.fg);
        }

        if (n.fd == nullptr)
        {
            this->fd = nullptr;
        }
        else
        {
            this->fd = new Noeud<T>(*n.fd);
        }
    }

    Noeud(Noeud<T> &&n) noexcept : info(std::move(n.info)), fg(n->fg), fd(n->fd)
    {
        n.fg = nullptr;
        n.fd = nullptr;
    }

    Noeud<T> &operator=(const Noeud<T> &n)
    {
        this->info = n.info;

        if (n.fg == nullptr)
        {
            this->fg = nullptr;
        }
        else
        {
            this->fg = new Noeud<T>(*n.fg);
        }

        if (n.fd == nullptr)
        {
            this->fd = nullptr;
        }
        else
        {
            this->fd = new Noeud<T>(*n.fd);
        }

        return *this;
    }

    Noeud<T> &operator=(Noeud<T> &&n) noexcept
    {
        this->info = std::move(n.info);

        this->fg = n.fg;
        this->fd = n.fd;
        n.fg = nullptr;
        n.fd = nullptr;

        return *this;
    }

    T getInfo() const { return this->info; }

    void setInfo(const T &newInfo) { this->info = newInfo; }

    Noeud<T> *getFD() const { return this->fd; }

    Noeud<T> *getFG() const { return this->fg; }

    void setFD(Noeud<T> *newFD) { this->fd = newFD; }

    void setFG(Noeud<T> *newFG) { this->fg = newFG; }

    ~Noeud()
    {
        delete this->fg;
        delete this->fd;
    };
};

template <typename T>
class ABR
{
private:
    int nb_element;
    Noeud<T> *r;

    bool find(const T &n, const Noeud<T> *o)
    {
        if (o == nullptr)
        {
            return false;
        }
        else if (n == o->getInfo())
        {
            return true;
        }
        else if (n > o->getInfo())
        {
            return find(n, o->getFD());
        }
        else if (n < o->getInfo())
        {
            return find(n, o->getFG());
        }

        return false;
    }

    Noeud<T> *insert(const T &n, Noeud<T> *o)
    {
        if (o == nullptr)
        {
            nb_element++;
            return new Noeud<T>(n);
        }

        if (n < o->getInfo())
        {
            o->setFG(insert(n, o->getFG()));
        }
        else if (n > o->getInfo())
        {
            o->setFD(insert(n, o->getFD()));
        }

        return o;
    }

public:
    ABR() : nb_element(0), r(nullptr) {}

    ABR(const ABR &a) : nb_element(a.nb_element)
    {
        this->r = new Noeud(a.r);
    }

    ABR(ABR &&a) noexcept : nb_element(std::move(a.nb_element)), r(a->r)
    {
        a.nb_element = 0;
        a->r = nullptr;
    }

    ABR &operator=(const ABR &a)
    {
        libererArbre(this->r);
        this->nb_element = a.nb_element;
        this->r = new Noeud(a.r);

        return *this;
    }

    ABR &operator=(ABR &&a) noexcept
    {
        libererArbre(this->r);
        this->nb_element = std::move(a.nb_element);
        this->r = a->r;

        a->r = nullptr;
        a.nb_element = 0;

        return *this;
    }

    bool find(const T &n)
    {
        return find(n, this->r);
    }

    void insert(const T &n)
    {
        r = insert(n, this->r);
        nb_element++;
    }

    void libererArbre(Noeud<T> *racine)
    {
        if (racine != nullptr)
        {
            libererArbre(racine->fg);
            libererArbre(racine->fd);
            delete racine;
        }
    }

    void afficherInfixe(Noeud<T> *n, std::ostream &os) const
    {
        if (n != nullptr)
        {
            afficherInfixe(n->getFG(), os);
            os << n->getInfo() << " ";
            afficherInfixe(n->getFD(), os);
        }
    }

    friend std::ostream &operator<<(std::ostream &os, const ABR<T> &arbre)
    {
        arbre.afficherInfixe(arbre.r, os);
        return os;
    }

    ~ABR()
    {
        delete this->r;
    }

    Noeud<T> *getRacine() { return this->r; }

    class iterator
    {
    private:
        std::stack<Noeud<T>> s;

    public:
        iterator(Noeud<T> *r)
        {
            Noeud<T> *temp = r;
            while (temp != nullptr)
            {
                s.push(*temp);
                temp = temp->getFG();
            }
        }

        iterator(Noeud<T> *r, bool caca)
        {
            s.push(*r);
        }

        T operator*() { return s.top().getInfo(); }

        void allFG(Noeud<T> * n) {
            while (n != nullptr)
            {
                s.push(*n);
                n = n->getFG();
            }
        }

        iterator& operator++()
        {
            Noeud<T> fd = this->s.top();
            this->s.pop();
            if(fd.getFD() != nullptr){
                this->allFG(fd.getFD());
            }
            return *this;
        }

        bool operator!=(iterator it)
        {
            return this->s.top().getInfo() != it.s.top().getInfo();
        }
    };

    iterator begin()
    {
        return iterator(this->getRacine());
    }

    iterator end()
    {
        Noeud<T> *temp = this->r;
        while (temp->getFD() != nullptr)
        {
            temp = temp->getFD();
        }
        return iterator(temp,true);
    }
};

#endif