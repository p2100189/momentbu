#ifndef ABR_H
#define ABR_H
#include <iostream>
using std::cout;

template <typename T>
class Node {
public:
    T data;
    Node * left;
    Node * right;
    // Constructeur par défaut
    Node() {
        // Flag for no data implemented
        this->data = -1;

        this->left = nullptr;
        this->right = nullptr;
    }
    void printNode() {
        cout << "Data: " << this->data << '\n';
    }

    // Constructeur par copie
    Node(const Node<T> & noeud_copie) {
        this->data = noeud_copie.data;
        if (this->left)
            this->left = new Node<T>(*noeud_copie.left);
        else
            this->left = nullptr;
        if (this->right)
            this->right = new Node<T>(*noeud_copie.right);
        else
            this->right = nullptr;
    }

    // Constructeur par mouvement
    Node(Node && noeud_copie) noexcept : data(std::move(noeud_copie.data)), left(noeud_copie.left), right(noeud_copie.right) {
        noeud_copie.left = nullptr;
        noeud_copie.right = nullptr;
    }

    // Opération d'affectation par copie
    Node & operator=(const Node<T> & noeud_affect) {
        this->data = noeud_affect.data;
        if (this->left)
            this->left = new Node<T>(*noeud_affect.left);
        else
            this->left = nullptr;
        if (this->right)
            this->right = new Node<T>(*noeud_affect.right);
        else
            this->right = nullptr;

        return *this;
    }

    ~Node() {
        this->left = nullptr;
        this->right = nullptr;
    }
};

template <typename T>
class ABR {
private:
    Node<T>* racine;
    int size;

    // Fonction récursive pour copier l'arbre
    Node<T>* copierArbre(Node<T>* racine_src) {
        if (racine_src == nullptr) return nullptr;
        Node<T>* nouveauNoeud = new Node<T>( *racine_src );
        nouveauNoeud->left = copierArbre(racine_src->left);
        nouveauNoeud->right = copierArbre(racine_src->right);
        return nouveauNoeud;
    }

    // Fonction récursive pour libérer l'arbre
    void libererArbre(Node<T>* racine) {
        if (racine != nullptr) {
            libererArbre(racine->left);
            libererArbre(racine->right);
            delete racine;
        }
    }

public:
    // Constructeur par défaut
    ABR() : racine(nullptr), size(0) {}

    // Constructeur par copie
    ABR(const ABR<T>& autre) : racine(nullptr), size(0) {
        racine = copierArbre(autre.racine);
        size = autre.size;
    }

    // Constructeur de mouvement
    ABR(ABR<T>&& autre) noexcept : racine(autre.racine), size(autre.size) {
        autre.racine = nullptr;
        autre.size = 0;
    }

    // Opérateur d'affectation par copie
    ABR<T>& operator=(const ABR<T>& autre) {
        if (this != &autre) {
            libererArbre(racine);  
            racine = copierArbre(autre.racine);
            size = autre.size;
        }
        return *this;
    }

    // Opérateur d'affectation par mouvement
    ABR<T>& operator=(ABR<T>&& autre) noexcept {
        if (this != &autre) {
            libererArbre(racine); 
            racine = autre.racine;
            size = autre.size;

            autre.racine = nullptr;
            autre.size = 0;
        }
        return *this;
    }

    // Destructeur
    ~ABR() {
        libererArbre(racine);
    }
};


#endif // ABR_H