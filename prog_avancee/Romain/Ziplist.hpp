#ifndef ZipList_H
#define ZipList_H

#include <iostream>

template <typename T>
class Cellule {
private:
    T info;
    Cellule * cellule_suivante;

public:
    // Constructeur par défaut
    Cellule() : info(T()), cellule_suivante(nullptr) {}

    // Constructeur par copie
    Cellule(const Cellule & autre) : info(autre.info) {
        if (autre.cellule_suivante != nullptr) {
            cellule_suivante = new Cellule(*autre.cellule_suivante);
        } else {
            cellule_suivante = nullptr;
        }
    }

    // Constructeur par mouvement
    Cellule(Cellule && autre) : info(move(autre.info)), cellule_suivante(autre.cellule_suivante) {
        autre.cellule_suivante = nullptr;
    }

    // Constructeur à partir d'un élément et d'un ptr
    Cellule(const T & elem, Cellule * ptr_vers_cellule) {
        this->info = elem;
        this->cellule_suivante = ptr_vers_cellule;
    }

    // Opérateur d'affectation par copie
    void operator=(const Cellule & autre) {
        info = autre.info;
        delete cellule_suivante;
        if (autre.cellule_suivante != nullptr)
            cellule_suivante = new Cellule(*autre.cellule_suivante);
        else
            cellule_suivante = nullptr;
    }

    // opérateur d'affectation par mouvement
    void operator=(Cellule && autre) {
        delete cellule_suivante;
        info = move(autre.info);
        cellule_suivante = autre.cellule_suivante;
        autre.cellule_suivante = nullptr;
    }

    // Destructeur
    ~Cellule() {
        delete cellule_suivante; // Libération de la mémoire de la cellule suivante
    }

    inline const T get_info() const { return this->info; }
    inline Cellule * get_suivante() const { return this->cellule_suivante; }
};

template <typename T>
class ZipList {
private:
    Cellule<T> * Cellule_tete;
    int taille_liste;

public:
    // Constructeur par défaut
    ZipList() : taille_liste(0), Cellule_tete(new Cellule<T>()) {}

    // Destructeur pour libérer la mémoire
    ~ZipList() {
        delete Cellule_tete; // Libérer la mémoire allouée pour la tête de liste
    }

    void push_front(const T & element_a_inserer) {
        if (this->taille_liste == 0) {
            Cellule_tete = new Cellule<T>(element_a_inserer, nullptr);
            taille_liste++;
        } else {
            Cellule<T> * temp = this->Cellule_tete;
            Cellule_tete = new Cellule<T>(element_a_inserer, temp);
            this->taille_liste++;
        }
    }

    const Cellule<T> * getRacine() const { return this->Cellule_tete; }

    void affiche_liste() {
        Cellule<T> * traverse = this->Cellule_tete;
        while (traverse != nullptr) {
            std::cout << "info : " << traverse->get_info() << std::endl;
            traverse = traverse->get_suivante();
        }
    }

    void affiche_liste(std::ostream & os, const Cellule<T> * Cellule_tete) const {
        const Cellule<T> * traverse = Cellule_tete;
        while (traverse != nullptr) {
            os << traverse->get_info();
            traverse = traverse->get_suivante();
        }
    }
    friend std::ostream & operator<<(std::ostream & os, const ZipList & liste_a_affiche) {
        liste_a_affiche.affiche_liste(os, liste_a_affiche.getRacine());
        return os;
    }

    const Cellule<T> * front() const {
        return this->Cellule_tete;
    }


    int size(){
        return this->taille_liste;
    }
};

#endif