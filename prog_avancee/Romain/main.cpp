#include "Ziplist.hpp"

int main(void) {
    ZipList<int> lili;
    for (int i = 0; i < 15; i++)
        lili.push_front(i % 4);

    std::cout << lili << std::endl;
}